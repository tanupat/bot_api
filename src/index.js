let express = require ('express');
var path = require('path');
let bodyParser = require('body-parser');
let routerTest1 = require('./router/test1');


const app = express();


app.use(bodyParser.json());

app.use((req, res , next) => {
  console.log(`${new Date().getHours().toString()}:${new Date().getMinutes().toString()}:${new Date().getSeconds().toString()} => ${req.originalUrl}`, req.body)
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next()
})

app.use(routerTest1);
app.use(express.static('public'));

app.listen(3000, () =>
  console.log('Server on port 3000!'),
);