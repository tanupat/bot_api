
const request = require('request');

function LINEnotify(msg) {
    // console.log("LINEnotify"+msg);
    
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {ORGH6Pdpy3hl8NqAG7jQz6ZQOdD6hvxYdjDul6c176Mqq+Cr1pvA0Mr9FN6MTv97JnWhuR1x/h/6BUHgS/aAmMGRGc7X0vO0Z6N2SUMamXWIBuCpq6isDt/4OQExggefwlYRkNN2LFlLUzEOqaJPHAdB04t89/1O/w1cDnyilFU=}'
    }
    let body = JSON.stringify({
        to: 'Ca46f7a1acf8d8c5822fa717cd84d9701',
        messages: [{
			"type": "flex",
			"altText": "Machine " ,
			"contents": {
				"type": "bubble",
				"body": {
					"type": "box",
					"layout": "vertical",
					"contents": [{
							"type": "text",
							"text": "Test",
							"weight": "bold",
							"color": "#ff0000",
							"size": "lg"
						},
						{
							"type": "text",
							"text": msg.message,
							"weight": "bold",
							"size": "xl",
							"margin": "md"
						},
						{
							"type": "separator",
							"margin": "xxl"
						},
						{
							"type": "box",
							"layout": "vertical",
							"margin": "xxl",
							"spacing": "sm",
							"contents": [{
									"type": "box",
									"layout": "horizontal",
									"contents": [{
											"type": "text",
											"text": "Time",
											"size": "sm",
											"color": "#555555",
											"flex": 0
										},
										{
											"type": "text",
											"text": "xxx",
											"size": "sm",
											"color": "#111111",
											"align": "end"
										}
									]
								},
								{
									"type": "box",
									"layout": "horizontal",
									"contents": [{
											"type": "text",
											"text": "OEE",
											"size": "sm",
											"color": "#555555",
											"flex": 0
										},
										{
											"type": "text",
											"text": "000"+"%",
											"size": "sm",
											"color": "#111111",
											"align": "end"
										}
									]
								},
								{
									"type": "separator",
									"margin": "xxl"
								},
								{
									"type": "box",
									"layout": "horizontal",
									"margin": "xxl",
									"contents": [{
											"type": "text",
											"text": "XXXX",
											"size": "sm",
											"color": "#555555"
										},
										{
											"type": "text",
											"text": "000",
											"size": "sm",
											"color": "#111111",
											"align": "end"
										}
									]
								},
								{
									"type": "box",
									"layout": "horizontal",
									"contents": [{
											"type": "text",
											"text": "XXXX",
											"size": "sm",
											"color": "#555555"
										},
										{
											"type": "text",
											"text": "000",
											"size": "sm",
											"color": "#111111",
											"align": "end"
										}
									]
								},
								{
									"type": "box",
									"layout": "horizontal",
									"contents": [{
											"type": "text",
											"text": "XXXX",
											"size": "sm",
											"color": "#555555"
										},
										{
											"type": "text",
											"text": "000",
											"size": "sm",
											"color": "#111111",
											"align": "end"
										}
									]
								},
								{
									"type": "box",
									"layout": "horizontal",
									"contents": [{
											"type": "text",
											"text": "XXXX",
											"size": "sm",
											"color": "#555555"
										},
										{
											"type": "text",
											"text": "000",
											"size": "sm",
											"color": "#111111",
											"align": "end"
										}
									]
								}
							]
						},
						{
							"type": "separator",
							"margin": "xxl"
						},
						{
							"type": "box",
							"layout": "horizontal",
							"margin": "md",
							"contents": [{
									"type": "text",
									"text": "MACHINE ID",
									"size": "xs",
									"color": "#aaaaaa",
									"flex": 0
								},
								{
									"type": "text",
									"text": "#"+msg.templa,
									"color": "#aaaaaa",
									"size": "xs",
									"align": "end"
								}
							]
						}
					]
				}
			}
		}]
    })
    request.post({
        url: 'https://api.line.me/v2/bot/message/push',
        headers: headers,
        body: body
    }, (err, res, body) => {
        console.log('statusLineNotify = ' + res.statusCode);
    });
}

module.exports = {
    LINEnotify
  };